let slider = document.querySelector('.slider-contenedor');
let sliderTest = document.querySelectorAll('.slider-test');
let contador = 1;
let tamanoWidth = sliderTest[0].clientWidth;
let intervalo = 2000;

slides();

function slides(){
    slider.style.transform = 'translate('+(- tamanoWidth * contador)+'px)';
    slider.style.transition = 'transform 2s';
    contador++;

    if(contador === sliderTest.length){
        contador=0;
        setTimeout(()=> {
            slider.style.transform = 'translate(0px)';
            slider.style.transition = 'transform 0s';
        },intervalo)
    }
}